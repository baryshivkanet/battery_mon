#include <ESP8266WiFi.h>
#include <InfluxDbClient.h>
#include <InfluxDbCloud.h>

constexpr char PRE_FIELD[]{ "A0002" };
constexpr char DB_URL[]{ "http://mon.ksm.in.ua:8086" };
constexpr char DB_TOKEN[]{ "................" };
constexpr char DB_BUCKET[]{ "battery01" };
constexpr char DB_ORG[]{ "baryshivka.net" };

constexpr char WIFI_SSID[]{ "Baryshivka.net" };
constexpr char WIFI_PASSWORD[]{ "..............." };

constexpr uint32_t sleep_delay_ms{ 1000 };

constexpr int read_pin{ A0 };

constexpr float VREF{ 3.2f };
constexpr float DIV_R1{ 30000.f };   // 30k
constexpr float DIV_R2{ 7500.f };    // 7.5k

uint8_t g_led_mode{ LOW };

InfluxDBClient db_client(DB_URL, DB_ORG, DB_BUCKET, DB_TOKEN, InfluxDbCloud2CACert);
Point battery_voltage("battery_voltage");

constexpr char TZ_INFO[]{ "EST5EDT" };

String field_name{ "volts_" };

void setup() {
  Serial.begin(115200);
  delay(10);

  pinMode(read_pin, INPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  Serial.print("Connecting to ");
  Serial.println(WIFI_SSID);

  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  timeSync(TZ_INFO, "pool.ntp.org", "time.nis.gov");

  if (db_client.validateConnection())
  {
    Serial.print("Connected to InfluxDB: ");
    Serial.println(db_client.getServerUrl());
  }
  else
    {
      Serial.print("InfluxDB connection failed: ");
      Serial.println(db_client.getLastErrorMessage());
    }

  field_name += PRE_FIELD;
}

void loop() {
  g_led_mode = g_led_mode == LOW ? HIGH : LOW;
  digitalWrite(LED_BUILTIN, g_led_mode);

  auto voltage = static_cast<float>(analogRead(read_pin)) * VREF * ((DIV_R1 + DIV_R2) / DIV_R2) / 1024.f;
  if (voltage < 1.f)
    voltage = 0.f;
  Serial.print("voltage=");
  Serial.println(voltage);

  battery_voltage.clearFields();
  battery_voltage.addField(field_name, voltage);

  if (!db_client.writePoint(battery_voltage))
  {
    Serial.print("InfluxDB write failed: ");
    Serial.println(db_client.getLastErrorMessage());
  }

  delay(sleep_delay_ms);
}
