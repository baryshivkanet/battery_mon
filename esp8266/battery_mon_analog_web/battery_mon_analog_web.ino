#include <ESP8266WiFi.h>
#include <InfluxDbClient.h>
#include <InfluxDbCloud.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>

ESP8266WebServer server(80);
String html_header = "<!DOCTYPE html>\
<html><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\
<head><title>Веб-интерфейс микроконтроллера</title><style>\
body { background-color: #F2F2F2; font-family: Arial, Helvetica, Sans-Serif; Color: #007F0E; }\
p { background-color: EFEFCB; border-radius: 10px; box-shadow: 0 0 7px 3px #00D318; padding: 10px 20px 10px 20px; margin: 20px auto; width: 500px; }\
</style></head>";

String _ssidStandard = "Battery_Monitor"; 
String _passwordStandard = "12345678";

void(* resetFunc) (void) = 0;   // Функция перезагрузки модуля
bool button = false;            // Режим сервисный выключён
String str = "";
boolean conf = false;


String db_URL;                  // Переменные настроек
String db_TOKEN;
String db_BUCKET;
String db_ORG;
String db_FIELD;
String sSleep;
String sReadPin;
String sSSIDclient;
String sPASSclient;
String sSSIDserver;
String sPASSserver;

int lArray = 500;               // Размер массива данных ("большой строки")
int ldb_URL = 10;               // Адресс каждой строки данных 
int ldb_TOKEN = 90;
int ldb_BUCKET = 240;
int ldb_ORG = 310;
int ldb_FIELD = 360;
int lsSleep = 370;
int lsReadPin = 375;
int lsSSIDclient = 380;
int lsPASSclient = 410;
int lsSSIDserver = 440;
int lsPASSserver = 470;

int sdb_URL = 80;               // Размер каждой строки данных 
int sdb_TOKEN = 150;
int sdb_BUCKET = 70;
int sdb_ORG = 50;
int sdb_FIELD = 10;
int ssSleep = 5;
int ssReadPin = 5;
int ssSSIDclient = 30;
int ssPASSclient = 30;
int ssSSIDserver = 30;
int ssPASSserver = 30;

constexpr float VREF{ 3.2f };
constexpr float DIV_R1{ 30000.f };   // 30k
constexpr float DIV_R2{ 7500.f };    // 7.5k
uint8_t g_led_mode{ LOW };

Point battery_voltage("battery_voltage");
constexpr char TZ_INFO[]{ "EST5EDT" };
String field_name{ "volts_" };


void setup() {
  Serial.begin(115200);                                                 // Скорость порта
  EEPROM.begin(lArray);
  pinMode(atoi(sReadPin.c_str()), INPUT);                               // Инициализация аналогового пина
  pinMode(4, INPUT_PULLUP);                                             // инициализация кнопки (пин D2)
  pinMode(LED_BUILTIN, OUTPUT);                                         // Инициализация встроеного светодиода

  readEEPROM();                                                         // Читаем параметры из памяти МК

  if (!digitalRead(4) == false) {                                       // обработчик положения кнопки (выключено)
    if(button == false){ 
      button = false;                                                   // режим сервисный выключён
      Serial.println("-------------------------------------------------");// сообщение
      Serial.println("Подключение к сети ... ");                        // сообщение
      Serial.print("SSID: ");     Serial.println(sSSIDclient);
      Serial.print("Password: "); Serial.println(sPASSclient);
      WiFi.mode(WIFI_STA);                                              // режим модуля - Клиент 
      WiFi.begin(sSSIDclient, sPASSclient);                             // Запуск клиента
      int count = 100;
      while (WiFi.status() != WL_CONNECTED) {                           // Подключение ...
        delay(200);                                                     // Задержка
        Serial.print(".");                                              // сообщение
        count -= 1;
        if (count <= 0) {
          Serial.println();                                             // сообщение
          Serial.print("Не удалось подключиться к сети WiFi: ");        // сообщение
          Serial.println(sSSIDclient);
          Serial.println("Микроконтроллер будет перезагружен");         // сообщение
          delay(500);                                                  // Задержка
          resetFunc();
        }
      }
      Serial.println();                                                 // сообщение
      Serial.println("Подключение к сети WiFi успешно");                // сообщение
      Serial.print("IP адрес сети: ");                                  // сообщение
      Serial.println(WiFi.localIP());                                   // сообщение - IP адрес сети

      timeSync(TZ_INFO, "pool.ntp.org", "time.nis.gov");                // Синхронизация времени
    } else  {
      Serial.println();                                                   // сообщение
      Serial.println("Неверно указан SSID {WiFi настройки (клиент роутера)}"); // сообщение
      digitalWrite(LED_BUILTIN, LOW);
    }
    Serial.println("-------------------------------------------------");  // сообщение
    field_name += db_FIELD;
  }

  if (!digitalRead(4) == true) {                                          // обработчик положения кнопки (включено)
    button = true;                                                        // режим: сервисный включён
    bool StartAP = false;                                                 // проверка на запуск точки доступа
    digitalWrite(LED_BUILTIN, LOW);                                       // светодиод светится непрерывно
    Serial.println("-------------------------------------------------");  // сообщение
    Serial.println("Конфигурация точки доступа ...");                     // сообщение
    WiFi.mode(WIFI_AP);                                                   // режим модуля - Точка доступа                
    Serial.println("Создание точки доступа ... ");                        // сообщение
    Serial.print("SSID: "); Serial.println(sSSIDserver);                  // сообщение - SSIID
    Serial.print("Password: "); Serial.println(sPASSserver);              // сообщение - PASSWORD    
    WiFi.softAP(sSSIDserver, sPASSserver) ? StartAP = true : StartAP = false;// успех/ошибка создания точки доступа 
    delay(1000);                                                          // Задержка
    if (StartAP == false) {     
      Serial.println("Ошибка запуска точки доступа");                     // сообщение
      Serial.println("Запуск аварийной точки доступа ...");               // сообщение
      Serial.print("SSID: "); Serial.println(_ssidStandard);              // сообщение - SSIID
      Serial.print("Password: "); Serial.println(_passwordStandard);      // сообщение - PASSWORD   
      WiFi.softAP(_ssidStandard, _passwordStandard) ? StartAP = true : StartAP = false;// успех/ошибка создания точки доступа 
      delay(1000);                                                        // Задержка
    }
    if (StartAP == true) {
      Serial.println("Точка доступа успешно запущена");                   // сообщение
      IPAddress ip_address = WiFi.softAPIP();                             // присвоивание IP адреса переменной
      server.on("/", handleRoot);                                         // Запуск точки доступа
      server.on("/ok", handleOk);                                         // Запуск точки доступа
      server.begin();                                                     // Запуск точки доступа
      Serial.print("IP адрес точки доступа: ");                           // сообщение
      Serial.println(ip_address);                                         // сообщение - ІР адрес
      Serial.println("Cервисный режим включён");                          // сообщение
      Serial.println("-------------------------------------------------");// сообщение
    }
  }
}


void loop() {
  bool buttonTemp = !digitalRead(4);                                    // читаем инвертированное значение положения кнопки

  if (button == false && buttonTemp == true) {                          // ( включение кнопки )
    button = true;                                                      // режим сервисный включён
    digitalWrite(LED_BUILTIN, LOW);                                     // светодиод светится непрерывно
    Serial.println("-------------------------------------------------");// сообщение
    Serial.println("!!! Кнопка зажата");                                // сообщение
    Serial.println("Вход в сервисный режим");                           // сообщение
    delay(1000);                                                        // задержка
    digitalWrite(LED_BUILTIN, HIGH);                                    // светодиод гасим
    resetFunc();                                                        // вызываем функцию перезагрузки
  }

  if (button == true && buttonTemp == false) {                          // ( выключение кнопки )
    button = false;                                                     // режим сервисный выключён
    digitalWrite(LED_BUILTIN, HIGH);                                    // светодиод гасим
    Serial.println("-------------------------------------------------");// сообщение
    Serial.println("Выход из сервисного режима");                       // сообщение
    delay(1000);                                                        // задержка
    resetFunc();                                                        // вызываем функцию перезагрузки
  }
  
  if (button == true && buttonTemp == true) {                           // ( кнопка включена )
    server.handleClient();
  }

  if (button == false && buttonTemp == false) {                         // ( кнопка выключена )

    if (WiFi.status() != WL_CONNECTED) {                                // Подключение ...
      Serial.println("Wi-Fi отключился, будет повторное подключение!"); // сообщение
      Serial.println("Задержка 10 секунд . . .");    // сообщение
      delay(1000);                                                      // Задержка
      WiFi.begin(sSSIDclient, sPASSclient);                             // Повторное подключение
      delay(9000);                                                      // Задержка
    } else {
      
      InfluxDBClient db_client(db_URL.c_str(), db_ORG.c_str(), db_BUCKET.c_str(), db_TOKEN.c_str(), InfluxDbCloud2CACert);
      delay(strtol(sSleep.c_str(),NULL,16)*1000);
      if (db_client.validateConnection()) {          // Подключение к серверу
        g_led_mode = g_led_mode == LOW ? HIGH : LOW;
        digitalWrite(LED_BUILTIN, g_led_mode);
        
        auto voltage = static_cast<float>(analogRead(atoi(sReadPin.c_str()))) * VREF * ((DIV_R1 + DIV_R2) / DIV_R2) / 1024.f;
        if (voltage < 1.f) voltage = 0.f;
        Serial.print("Voltage = ");
        Serial.println(voltage);

        battery_voltage.clearFields();
        battery_voltage.addField(field_name, voltage);
      
        if (!db_client.writePoint(battery_voltage))
        {
          Serial.print("InfluxDB write failed: ");
          Serial.println(db_client.getLastErrorMessage());
        }
      } else {
        Serial.print("Подключение к серверу InfluxDB потеряно: ");      // сообщение
        Serial.println(db_client.getLastErrorMessage());                // сообщение - ошибка подключения
      }
    } 
  }
}



void handleRoot() {                                                     // Вывод HTTP
  String str = "";
  str += html_header;
  str += "<body><center><form action=\"ok\" method=\"post\" name=\"form\">\
<table><tr><td><b><u>Настройки базы данных</u></b></td></tr>\
<tr><td>DB_URL [< 80]:</td><td> <input name=\"DB_URL\" type=\"text\" value=\""; str += db_URL; str += "\"></td></tr>\
<tr><td>DB_TOKEN [< 150]:</td><td> <input name=\"DB_TOKEN\" type=\"text\" value=\""; str += db_TOKEN; str += "\"></td></tr>\
<tr><td>DB_BUCKET [< 70]:</td><td> <input name=\"DB_BUCKET\" type=\"text\" value=\""; str += db_BUCKET; str += "\"></td></tr>\
<tr><td>DB_ORG [< 50]:</td><td> <input name=\"DB_ORG\" type=\"text\" value=\""; str += db_ORG; str += "\"></td></tr>\
<tr><td>PRE_FIELD [< 10]:</td><td> <input name=\"PRE_FIELD\" type=\"text\" value=\""; str += db_FIELD; str += "\"></td></tr>\
<tr><td><br/><b><u>Настройки микроконтроллера</u></b></td></tr>\
<tr><td>Sleep_Delay (sec) [< 5]:</td><td> <input name=\"Sleep\" type=\"text\" value=\""; str += sSleep; str += "\"></td></tr>\
<tr><td>Read_Pin [< 5]:</td><td> <input name=\"Rpin\" type=\"text\" value=\""; str += sReadPin; str += "\"></td></tr>\
<tr><td><br/><b><u>WiFi настройки (клиент роутера)</u></b></td></tr>\
<tr><td>WIFI_SSID [< 30]:</td><td> <input name=\"WC_SSID\" type=\"text\" value=\""; str += sSSIDclient; str += "\"></td></tr>\
<tr><td>WIFI_PASSWORD [< 30]:</td><td> <input name=\"WC_PASSWORD\" type=\"text\" value=\""; str += sPASSclient; str += "\"></td></tr>\
<tr><td><br/><b><u>WiFi настройки (точка доступа)</u></b></td></tr>\
<tr><td>SSID [< 30]:</td><td> <input name=\"SSID\" type=\"text\" value=\""; str += sSSIDserver; str += "\"></td></tr>\
<tr><td>PASSWORD [< 30]:</td><td> <input name=\"PASSWORD\" type=\"text\" value=\""; str += sPASSserver; str += "\"></td></tr>\
<tr><th><br/><input type=\"submit\" name=\"save\" value=\"Save\"></th></tr></table>\
</form></center></body></html>";
  server.send(300, "text/html", str);
}

void handleOk(){                                                        // Считать данные с HTTP
  db_URL = server.arg(0);
  db_TOKEN = server.arg(1);
  db_BUCKET = server.arg(2);
  db_ORG = server.arg(3);
  db_FIELD = server.arg(4);
  sSleep = server.arg(5);
  sReadPin = server.arg(6);
  sSSIDclient = server.arg(7);
  sPASSclient = server.arg(8);
  sSSIDserver = server.arg(9);
  sPASSserver = server.arg(10);
  
  bool errorPASS = false;
  if (sPASSclient.length() < 8 || sPASSserver.length() < 8) errorPASS = true;
  String str = "";
  str += html_header;
  str += "<body>";
  if(db_URL == "" || db_TOKEN == "" || db_BUCKET == "" || db_ORG == "" || db_FIELD == "" || sSleep == "" || sReadPin == "" || sSSIDclient == "" || sPASSclient == "" || sSSIDserver == "" || sPASSserver == "") {
    str += "<p>Внесены не все параметры настроек!</p>\
    <center><p><a href=\"\/\">Главная страница</a></p></center>";
  } else if (errorPASS == true) {
    str += "<p>Пароль от Wi-Fi сети должен содержать минимум 8 символов!</p>\
    <center><p><a href=\"\/\">Главная страница</a></p></center>";
  } else {
    str +="<p>Данные внесены корректно и будут внесены в память микроконтроллера!</br></br>\
    Что бы изменения вступили в силу, микроконтроллер будет перезагружен! \
    Для этого переключите положение кнопки в исходное состояние.</p>\
    <center><p><a href=\"\/\">Главная страница</a></p></center>";
    
    Serial.println();
    Serial.println("-------------------------------------------------");// сообщение
    Serial.println("Данные внесены корректно");                         // сообщение
    Serial.print("DB_URL: ");       Serial.println(db_URL);
    Serial.print("DB_TOKEN: ");     Serial.println(db_TOKEN);
    Serial.print("DB_BUCKET: ");    Serial.println(db_BUCKET);
    Serial.print("DB_ORG: ");       Serial.println(db_ORG);
    Serial.print("PRE_FIELD: ");    Serial.println(db_FIELD);
    Serial.print("Sleep: ");        Serial.println(sSleep);
    Serial.print("Rpin: ");         Serial.println(sReadPin);
    Serial.print("WC_SSID: ");      Serial.println(sSSIDclient);
    Serial.print("WC_PASSWORD: ");  Serial.println(sPASSclient);
    Serial.print("SSID: ");         Serial.println(sSSIDserver);
    Serial.print("PASSWORD: ");     Serial.println(sPASSserver);
    Serial.println("-------------------------------------------------");// сообщение
  }
  
  str += "</body></html>";
  server.send(200, "text/html", str);
  writeEEPROM();                                                        // Записать данные в память MK
}



void readEEPROM() {                                                     // Функция для чтения параметров из памяти МК
  char WriteArray[lArray];                                              // Создаем массив
  for (int i = 0; i < lArray; i++) WriteArray[i] = '\0';                // Обнуляем массив

  char wdb_URL[sdb_URL];                                                // Создаем масивы для каждого параметра
  char wdb_TOKEN[sdb_TOKEN];
  char wdb_BUCKET[sdb_BUCKET];
  char wdb_ORG[sdb_ORG];
  char wdb_FIELD[sdb_FIELD];
  char wsSleep[ssSleep];
  char wsReadPin[ssReadPin];
  char wsSSIDclient[ssSSIDclient];
  char wsPASSclient[ssPASSclient];
  char wsSSIDserver[ssSSIDserver];
  char wsPASSserver[ssPASSserver];

  for (int i = 0; i < lArray; i++) WriteArray[i] = EEPROM.read(i);      // Читаем всё из памяти МК и записываем в "большую строку"

  for (int i = 0; i < sdb_URL; i++)       wdb_URL[i] = '\0';            // Обнуляем массив
  for (int i = 0; i < sdb_TOKEN; i++)     wdb_TOKEN[i] = '\0';
  for (int i = 0; i < sdb_BUCKET; i++)    wdb_BUCKET[i] = '\0';
  for (int i = 0; i < sdb_ORG; i++)       wdb_ORG[i] = '\0';
  for (int i = 0; i < sdb_FIELD; i++)     wdb_FIELD[i] = '\0';
  for (int i = 0; i < ssSleep; i++)       wsSleep[i] = '\0';
  for (int i = 0; i < ssReadPin; i++)     wsReadPin[i] = '\0';
  for (int i = 0; i < ssSSIDclient; i++)  wsSSIDclient[i] = '\0';
  for (int i = 0; i < ssPASSclient; i++)  wsPASSclient[i] = '\0';
  for (int i = 0; i < ssSSIDserver; i++)  wsSSIDserver[i] = '\0';
  for (int i = 0; i < ssPASSserver; i++)  wsPASSserver[i] = '\0';

  for (int i = 0; i < sdb_URL; i++)       wdb_URL[i] = WriteArray[i+ldb_URL]; // Из "большой строки" разбиваем на малые массивы
  for (int i = 0; i < sdb_TOKEN; i++)     wdb_TOKEN[i] = WriteArray[i+ldb_TOKEN]; 
  for (int i = 0; i < sdb_BUCKET; i++)    wdb_BUCKET[i] = WriteArray[i+ldb_BUCKET];
  for (int i = 0; i < sdb_ORG; i++)       wdb_ORG[i] = WriteArray[i+ldb_ORG];
  for (int i = 0; i < sdb_FIELD; i++)     wdb_FIELD[i] = WriteArray[i+ldb_FIELD];
  for (int i = 0; i < ssSleep; i++)       wsSleep[i] = WriteArray[i+lsSleep];
  for (int i = 0; i < ssReadPin; i++)     wsReadPin[i] = WriteArray[i+lsReadPin];
  for (int i = 0; i < ssSSIDclient; i++)  wsSSIDclient[i] = WriteArray[i+lsSSIDclient];
  for (int i = 0; i < ssPASSclient; i++)  wsPASSclient[i] = WriteArray[i+lsPASSclient];
  for (int i = 0; i < ssSSIDserver; i++)  wsSSIDserver[i] = WriteArray[i+lsSSIDserver];
  for (int i = 0; i < ssPASSserver; i++)  wsPASSserver[i] = WriteArray[i+lsPASSserver];

  db_URL = String(wdb_URL);                                             // Записываем параметры в переменные
  db_TOKEN = String(wdb_TOKEN);
  db_BUCKET = String(wdb_BUCKET);
  db_ORG = String(wdb_ORG);
  db_FIELD = String(wdb_FIELD);
  sSleep = String(wsSleep);
  sReadPin = String(wsReadPin);
  sSSIDclient = String(wsSSIDclient);
  sPASSclient = String(wsPASSclient);
  sSSIDserver = String(wsSSIDserver);
  sPASSserver = String(wsPASSserver);

  Serial.println();
  Serial.println("-------------------------------------------------");// сообщение
  Serial.print("DB_URL: ");       Serial.println(db_URL);
  Serial.print("DB_TOKEN: ");     Serial.println(db_TOKEN);
  Serial.print("DB_BUCKET: ");    Serial.println(db_BUCKET);
  Serial.print("DB_ORG: ");       Serial.println(db_ORG);
  Serial.print("PRE_FIELD: ");    Serial.println(db_FIELD);
  Serial.print("Sleep: ");        Serial.println(sSleep);
  Serial.print("Rpin: ");         Serial.println(sReadPin);
  Serial.print("WC_SSID: ");      Serial.println(sSSIDclient);
  Serial.print("WC_PASSWORD: ");  Serial.println(sPASSclient);
  Serial.print("SSID: ");         Serial.println(sSSIDserver);
  Serial.print("PASSWORD: ");     Serial.println(sPASSserver);
}

void writeEEPROM() {                                                    // Функция для записи данных в память МК
  char WriteArray[lArray];                                              // Создаем массив
  for (int i = 0; i < lArray; i++) WriteArray[i] = '\0';                // Обнуляем массив
  for (int i = 0; i < lArray; i++) EEPROM.put(i, '\0');                 // Обнуляем память МК

  char wdb_URL[db_URL.length()];                                        // Создаем масивы для каждого параметра
  char wdb_TOKEN[db_TOKEN.length()];
  char wdb_BUCKET[db_BUCKET.length()];
  char wdb_ORG[db_ORG.length()];
  char wdb_FIELD[db_FIELD.length()];
  char wsSleep[sSleep.length()];
  char wsReadPin[sReadPin.length()];
  char wsSSIDclient[sSSIDclient.length()];
  char wsPASSclient[sPASSclient.length()];
  char wsSSIDserver[sSSIDserver.length()];
  char wsPASSserver[sPASSserver.length()];

  db_URL.toCharArray(wdb_URL, db_URL.length()+1);                       // Записываем строки с параметрами в новосозданные масивы
  db_TOKEN.toCharArray(wdb_TOKEN, db_TOKEN.length()+1);
  db_BUCKET.toCharArray(wdb_BUCKET, db_BUCKET.length()+1);
  db_ORG.toCharArray(wdb_ORG, db_ORG.length()+1);
  db_FIELD.toCharArray(wdb_FIELD, db_FIELD.length()+1);
  sSleep.toCharArray(wsSleep, sSleep.length()+1);
  sReadPin.toCharArray(wsReadPin, sReadPin.length()+1);
  sSSIDclient.toCharArray(wsSSIDclient, sSSIDclient.length()+1);
  sPASSclient.toCharArray(wsPASSclient, sPASSclient.length()+1);
  sSSIDserver.toCharArray(wsSSIDserver, sSSIDserver.length()+1);
  sPASSserver.toCharArray(wsPASSserver, sPASSserver.length()+1);

  for (int i = ldb_URL;      i < ldb_URL+db_URL.length(); i++)           WriteArray[i] = wdb_URL[i-ldb_URL];  // Создаем "большую строку" из параметров
  for (int i = ldb_TOKEN;    i < ldb_TOKEN+db_TOKEN.length(); i++)       WriteArray[i] = wdb_TOKEN[i-ldb_TOKEN];
  for (int i = ldb_BUCKET;   i < ldb_BUCKET+db_BUCKET.length(); i++)     WriteArray[i] = wdb_BUCKET[i-ldb_BUCKET];
  for (int i = ldb_ORG;      i < ldb_ORG+db_ORG.length(); i++)           WriteArray[i] = wdb_ORG[i-ldb_ORG];
  for (int i = ldb_FIELD;    i < ldb_FIELD+db_FIELD.length(); i++)       WriteArray[i] = wdb_FIELD[i-ldb_FIELD];
  for (int i = lsSleep;      i < lsSleep+sSleep.length(); i++)           WriteArray[i] = wsSleep[i-lsSleep];
  for (int i = lsReadPin;    i < lsReadPin+sReadPin.length(); i++)       WriteArray[i] = wsReadPin[i-lsReadPin];
  for (int i = lsSSIDclient; i < lsSSIDclient+sSSIDclient.length(); i++) WriteArray[i] = wsSSIDclient[i-lsSSIDclient];
  for (int i = lsPASSclient; i < lsPASSclient+sPASSclient.length(); i++) WriteArray[i] = wsPASSclient[i-lsPASSclient];
  for (int i = lsSSIDserver; i < lsSSIDserver+sSSIDserver.length(); i++) WriteArray[i] = wsSSIDserver[i-lsSSIDserver];
  for (int i = lsPASSserver; i < lsPASSserver+sPASSserver.length(); i++) WriteArray[i] = wsPASSserver[i-lsPASSserver];

  for (int i = 0; i < lArray; i++) {
    EEPROM.put(i, WriteArray[i]);                                       // Записываем "большую строку" в память МК
    delay(2);
  }

  Serial.println();
  if (EEPROM.commit()) { Serial.println("Запись прошла успешно!"); }
    else { Serial.println("Запись прошла с ошибками!"); }
}
