#include <Adafruit_INA219.h>
#include <ESP8266WiFi.h>
#include <InfluxDbClient.h>
#include <InfluxDbCloud.h>

#define _DEBUG
#define __VERSION "1.0"

constexpr char PRE_FIELD[]{ "D0002" };
constexpr char DB_URL[]{ "http://mon.ksm.in.ua:8086" };
constexpr char DB_TOKEN[]{ "......................" };
constexpr char DB_BUCKET[]{ "battery01" };
constexpr char DB_ORG[]{ "baryshivka.net" };

constexpr char WIFI_SSID[]{ "Baryshivka.net" };
constexpr char WIFI_PASSWORD[]{ "........" };

constexpr uint32_t sleep_delay_ms{ 1000 };

Adafruit_INA219 ina219;
float mea_shuntvoltage = 0.f;
float mea_busvoltage = 0.f;
float mea_current_mA = 0.f;
float mea_loadvoltage = 0.f;
float mea_power_mW = 0.f;

uint8_t g_led_mode{ LOW };
const uint32_t UART_speed = 115200U;

InfluxDBClient db_client(DB_URL, DB_ORG, DB_BUCKET, DB_TOKEN, InfluxDbCloud2CACert);
Point battery_name("battery_info");

constexpr char TZ_INFO[]{ "EST5EDT" };

String field_name_shuntvoltage{ "shuntvoltage_" };
String field_name_busvoltage{ "busvoltage_" };
String field_name_current_mA{ "current_mA_" };
String field_name_loadvoltage{ "loadvoltage_" };
String field_name_power_mW{ "power_mW_" };

void setup() {
  delay(100);
  Serial.begin(UART_speed);
  while(!Serial){}
  delay(500);

  pinMode(LED_BUILTIN, OUTPUT);

  Serial.begin(UART_speed);
  Serial.println("Booting..");
  Serial.print("Version (INA219): ");
  Serial.println(__VERSION);
  Serial.print("Compiled: ");
  Serial.print(__DATE__);
  Serial.print(" ");
  Serial.println(__TIME__);

  if (!ina219.begin())
  {
    Serial.println("Failed to find INA219 chip");
    while (1) { delay(10); }
  }

  delay(500);

  Serial.print("Connecting to ");
  Serial.println(WIFI_SSID);

  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  timeSync(TZ_INFO, "pool.ntp.org", "time.nis.gov");

  if (db_client.validateConnection())
  {
    Serial.print("Connected to InfluxDB: ");
    Serial.println(db_client.getServerUrl());
  }
  else
    {
      Serial.print("InfluxDB connection failed: ");
      Serial.println(db_client.getLastErrorMessage());
    }

  field_name_shuntvoltage += PRE_FIELD;
  field_name_busvoltage += PRE_FIELD;
  field_name_current_mA += PRE_FIELD;
  field_name_loadvoltage += PRE_FIELD;
  field_name_power_mW += PRE_FIELD;
  
}

void updateVoltage()
{
  mea_shuntvoltage = ina219.getShuntVoltage_mV();
  mea_busvoltage = ina219.getBusVoltage_V();
  mea_current_mA = ina219.getCurrent_mA();
  mea_power_mW = ina219.getPower_mW();
  mea_loadvoltage = mea_busvoltage + (mea_shuntvoltage / 1000);

  if (mea_shuntvoltage < 0.f)
    mea_shuntvoltage = 0.f;
  if (mea_busvoltage < 0.f)
    mea_busvoltage = 0.f;
  if (mea_current_mA < 0.f)
    mea_current_mA = 0.f;
  if (mea_power_mW < 0.f)
    mea_power_mW = 0.f;
  if (mea_loadvoltage < 0.f)
    mea_loadvoltage = 0.f;
}

void loop() {
  g_led_mode = g_led_mode == LOW ? HIGH : LOW;
  digitalWrite(LED_BUILTIN, g_led_mode);

  updateVoltage();
#ifdef _DEBUG
  Serial.print("Bus Voltage:   "); Serial.print(mea_busvoltage); Serial.println(" V");
  Serial.print("Shunt Voltage: "); Serial.print(mea_shuntvoltage); Serial.println(" mV");
  Serial.print("Load Voltage:  "); Serial.print(mea_loadvoltage); Serial.println(" V");
  Serial.print("Current:       "); Serial.print(mea_current_mA); Serial.println(" mA");
  Serial.print("Power:         "); Serial.print(mea_power_mW); Serial.println(" mW");
  Serial.println("");
#endif  // _DEBUG

  battery_name.clearFields();
  battery_name.addField(field_name_shuntvoltage, mea_busvoltage);
  battery_name.addField(field_name_busvoltage, mea_shuntvoltage);
  battery_name.addField(field_name_current_mA, mea_loadvoltage);
  battery_name.addField(field_name_loadvoltage, mea_current_mA);
  battery_name.addField(field_name_power_mW, mea_power_mW);

  if (!db_client.writePoint(battery_name))
  {
    Serial.print("InfluxDB write failed: ");
    Serial.println(db_client.getLastErrorMessage());
  }

  delay(sleep_delay_ms);
}
